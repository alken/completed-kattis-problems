import java.util.Scanner;

public class SpeedLimit {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = 0;
        while (true) {
            int total = 0;
            n = input.nextInt();
            if (n < 0) {
                break;
            }
            input.nextLine();
            int s = 0;
            int t = 0;
            int temp = 0;
            for (int i = 0; i < n; i++) {
                temp = t;
                s = input.nextInt();
                t = input.nextInt();
                input.nextLine();
                total += s * (t - temp);
            }
            System.out.println(total + " miles");
        }
        input.close();
    }
}
