import java.util.Scanner;

public class LastFactorialDigit {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for (int i = 0; i < n; i++) {
            input.nextLine();
            int x = input.nextInt();
            int y = 1;
            for (int j = 1; j <= x; j++) {
                y *= j;
            }
            System.out.println(y % 10);
        }
        input.close();
    }
}
