import java.util.Scanner;

public class PieceOfCake {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int h = input.nextInt();
        int v = input.nextInt();
        int x = 0;
        int y = 0;
        if (n / 2 >= h) {
            x = n - h;
        }
        else {
            x = h;
        }
        if (n / 2 >= v) {
            y = n - v;
        }
        else {
            y = v;
        }
        System.out.println((x) * (y) * 4);
        input.close();
    }
}
