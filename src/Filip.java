import java.util.Scanner;

public class Filip {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int x = 0;
        int y = 0;
        while (a != 0) {
            x *= 10;
            x += a % 10;
            a /= 10;
        }
        while (b != 0) {
            y *= 10;
            y += b % 10;
            b /= 10;
        }
        System.out.println(Math.max(x, y));
        input.close();
    }
}
