import java.util.Scanner;

public class Oddities {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for (int i = 0; i < n; i++) {
            input.nextLine();
            int x = input.nextInt();
            System.out.println(x + " is " + (x % 2 == 0 ? "even" : "odd"));
        }
        input.close();
    }
}
