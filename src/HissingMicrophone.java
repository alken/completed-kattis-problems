import java.util.Scanner;

public class HissingMicrophone {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.next();
        char[] charray = s.toCharArray();
        boolean oneS = false;
        boolean twoS = false;
        for (char c : charray) {
            if (c == 's') {
                if (oneS) {
                    twoS = true;
                    break;
                } else {
                    oneS = true;
                }
            } else {
                oneS = false;
            }
        }
        System.out.println(twoS ? "hiss" : "no hiss");
        input.close();
    }
}
