import java.util.Scanner;

public class NoDuplicates {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        String[] sArray = s.split(" ");
        boolean flag = true;
        for (int i = 0; i < sArray.length; i++) {
            String x = sArray[i];
            for (int j = 0; j < sArray.length; j++) {
                if (i == j) {
                    continue;
                }
                String y = sArray[j];
                if (x.equals(y)) {
                    flag = false;
                    break;
                }
            }
            if (!flag) {
                break;
            }
        }
        System.out.println(flag ? "yes" : "no");
        input.close();
    }
}
