import java.util.Scanner;

public class Pot {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        input.nextLine();
        int x = 0;
        for (int i = 0; i < n; i++) {
            int p = input.nextInt();
            int pow = p % 10;
            int num = p / 10;
            int temp = (int)Math.pow(num, pow);
            x += temp;
            input.nextLine();
        }
        System.out.println(x);
        input.close();
    }
}
