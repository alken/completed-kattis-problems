import java.util.Scanner;

public class IsItHalloween {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.next();
        int n = input.nextInt();
        if ((s.equals("OCT") && n == 31) || (s.equals("DEC") && n == 25)) {
            System.out.println("yup");
        }
        else {
            System.out.println("nope");
        }
        input.close();
    }
}
