import java.util.Scanner;

public class TakeTwoStones {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        boolean flag = (n % 2 == 0);
        System.out.println(flag ? "Bob" : "Alice");
        input.close();
    }
}