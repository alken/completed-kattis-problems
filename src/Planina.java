import java.util.Scanner;

public class Planina {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int x = n + 1;
        int y = ((int)Math.pow(2, x) + 2) / 2;
        int z = (int)Math.pow(y, 2);
        System.out.println(z);
        input.close();
    }
}
